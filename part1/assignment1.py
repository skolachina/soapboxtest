#!/usr/bin/python
# -*- coding: utf-8 -*-

import io
import os
import sys
import itertools
import pandas as pd
from collections import Counter
 
data_dir = "/home/sudheer/Dropbox/sudheer/workspace/soapbox/SBL_NLP_LM_Exercise/data/train"

def read_label_file(filename):
    df = pd.read_csv(os.path.join(data_dir,filename),sep='\s+',index_col=None,header=None)
    df_word = df[df.iloc[:,0].str.endswith('word>')]
    df_phone = df[df.iloc[:,0].str.endswith('phone>')] 
    sentence = df_word.iloc[:,1].tolist()
    utterance = df_phone.iloc[:,1].tolist()
    return [sentence, utterance]
     
def extract_corpus():
    corpus = {}  
    for f in filter(None,os.listdir(data_dir)):
        corpus[f] = read_label_file(f)
    
    return corpus 

corpus = extract_corpus()
sentences = [corpus[k][0] for k in corpus.keys()]
utterances = [corpus[k][1] for k in corpus.keys()]
with open('corpus.txt','w') as f:
    for s in sentences:
        f.write(' '.join(s)+'\n')
f.close()

tokens = [word.lower() for sent in sentences for word in sent]
FreqDict = Counter(tokens)
freqdict_df = pd.DataFrame(sorted(FreqDict.items(),key=lambda k:k[1],reverse=True))
freqdict_df.to_csv('corpus.dict',sep=',',index=False,header=False)

speakers = []
sentences = []
for k in corpus.keys():
    speakers.append(k[:4])
    sentences.append(k[4:len(k)-5])

print "Number of unique speakers: %d"%(len(set(speakers)))
print "Number of unique sentences: %d"%(len(set(sentences)))
