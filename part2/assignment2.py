#!/usr/bin/python
# -*- coding: utf-8 -*-

import io
import os
import sys
 
data_dir = "/home/sudheer/Dropbox/sudheer/workspace/soapbox/SBL_NLP_LM_Exercise/gutenberg_potter"

def clean_text(text):
    text = text.replace("[Illustration]",'') #Deleting Illustrations and random characters
    text = text.replace('\r','')
    text = text.replace('_','')
    text = text.replace('--','')
    text = text.replace('(','( ') #inserting spaces between punctuation characters 
    text = text.replace(')',' )')
    text = text.replace('.',' .')
    text = text.replace('"',' " ')
    text = text.replace('.',' .')
    textlines = text.split('\n')
    start, end = 0, 0
    for i,t in enumerate(textlines):
        if t.startswith('*** START OF THIS PROJECT GUTENBERG EBOOK'):  #header
            start = i  
        if t.startswith('*** END OF THIS PROJECT GUTENBERG EBOOK'):   #footer
            end = i

    storytext = filter(None,textlines[start+1:end-start]) #remover header and footer

    return ' '.join(storytext).lower() #converting text to lower case to make vocabulary less sparse

def read_corpusfile(filename):
    text = open(os.path.join(data_dir,filename), 'r').read()
    return clean_text(text)    

def create_train_data():
    corpus = {}
    for f in filter(None, os.listdir(data_dir)): #iterate over all files in corpus dir
        corpus[f] = read_corpusfile(f)
    
    train = ' '.join(corpus.values()[0:int(len(corpus.keys())*0.6)+1])
    valid = ' '.join(corpus.values()[int(0.6*len(corpus.keys()))+1:int(0.8*len(corpus.keys()))+1])
    test = ' '.join(corpus.values()[int(0.8*len(corpus.keys())):])

    with open('potter.train.txt','w') as f1:
        f1.write(train)
    f1.close()

    with open('potter.valid.txt','w') as f2:
        f2.write(valid)
    f2.close()

    with open('potter.test.txt','w') as f3:
        f3.write(test)
    f3.close()

        
def rnn_language_model():
    if "potter.model.hidden100.class100.txt" in os.listdir("."): #If model exists, delete it
        os.system("rm -f potter.model.hidden100.class100.txt")
    os.system("time rnnlm-0.3e/rnnlm -train potter.train.txt -valid potter.valid.txt -rnnlm potter.model.hidden100.class100.txt -hidden 100 -rand-seed 1 -debug 2 -class 100 -bptt 4 -bptt-block 10") # train neural language model using Mikolov's library with following parameters- hidden layer size=100 neurons, class size=100, back propagation steps=4
    os.system("srilm/bin/i686-m64/ngram-count -text potter.train.txt -order 3 -lm temp/templm -gt3min 1 -gt4min 1 -kndiscount -interpolate -unk") # extract ngram counts using SRILM to train traditional count based lm with order 3 to get trigram model 
    os.system("srilm/bin/i686-m64/ngram -lm temp/templm -order 3 -ppl potter.test.txt -debug 2 > temp/temp.ppl -unk") 
    os.system("rnnlm-0.3e/convert <temp/temp.ppl >temp/ngram.txt") # Convert rnn model to srilm format
    os.system("time rnnlm-0.3e/rnnlm -rnnlm potter.model.hidden100.class100.txt -test potter.test.txt -lm-prob temp/ngram.txt -lambda 0.5") #results of the lm on the test set, perplexity of neural net, srilm model and combined


create_train_data()
rnn_language_model()
